package izht.agoda.formula1;

/**
 * A formula-1 car.
 * 
 * @author Boonyachart Kanchanabul
 */
public class FormulaOneCar implements Comparable<FormulaOneCar> {

   public static final double HANDLING_FACTOR = 0.8;
   private int team;
   private int topSpeed;
   private double acceleration;
   private double previousVelocity;
   private double velocity;
   private double position;
   private boolean isNitroUsed;

   /**
    * Constructor method.
    * 
    * @param i the team number
    */
   public FormulaOneCar(int i, FormulaOneCar previousCar) {
      this.team = i;
      this.topSpeed = 150 + 10 * i;
      this.acceleration = 2 * i;
      this.position = previousCar == null ? 0 : previousCar.position - (200 * i);
      this.previousVelocity = 0;
   }

   /**
    * Accelerate the car.
    * 
    * @param second time in second
    */
   public void accelerate(int second) {
      if (velocity < topSpeed) {
         velocity = velocity + acceleration * second;
         if (velocity > topSpeed)
            velocity = topSpeed;
      }
   }

   /**
    * Instantly decrease speed of the car.
    */
   public void handling() {
      velocity = HANDLING_FACTOR * velocity;
   }

   /**
    * Instantly double the speed of the car, can only use once.
    */
   public void nitro() {
      if (!isNitroUsed) {
         velocity = velocity * 2 > topSpeed ? topSpeed : velocity * 2;
         isNitroUsed = true;
      }
   }

   /**
    * Calculate new position of the car.
    * 
    * @param startingVelocity starting speed
    * @param endingVelocity ending speed
    * @param second time in second
    * @return new position in meters
    */
   public double drive(int second) {
      position = position + (previousVelocity + velocity) / 2 * second;
      previousVelocity = velocity;
      return position;
   }

   @Override
   public int compareTo(FormulaOneCar o) {
      return (int)(o.position - this.position);
   }

   /**
    * Gets team.
    * 
    * @return the team
    */
   public int getTeam() {
      return team;
   }

   /**
    * Gets topSpeed.
    * 
    * @return the topSpeed
    */
   public int getTopSpeed() {
      return topSpeed;
   }

   /**
    * Gets acceleration.
    * 
    * @return the acceleration
    */
   public double getAcceleration() {
      return acceleration;
   }

   /**
    * Gets previousVelocity.
    * 
    * @return the previousVelocity
    */
   public double getPreviousVelocity() {
      return previousVelocity;
   }

   /**
    * Gets velocity.
    * 
    * @return the velocity
    */
   public double getVelocity() {
      return velocity;
   }

   /**
    * Gets position.
    * 
    * @return the position
    */
   public double getPosition() {
      return position;
   }

   /**
    * Gets isNitroUsed.
    * 
    * @return the isNitroUsed
    */
   public boolean isNitroUsed() {
      return isNitroUsed;
   }

   /**
    * Set team.
    *
    * @param team the team to set
    */
   public void setTeam(int team) {
      this.team = team;
   }

   /**
    * Set topSpeed.
    *
    * @param topSpeed the topSpeed to set
    */
   public void setTopSpeed(int topSpeed) {
      this.topSpeed = topSpeed;
   }

   /**
    * Set acceleration.
    *
    * @param acceleration the acceleration to set
    */
   public void setAcceleration(double acceleration) {
      this.acceleration = acceleration;
   }

   /**
    * Set previousVelocity.
    *
    * @param previousVelocity the previousVelocity to set
    */
   public void setPreviousVelocity(double previousVelocity) {
      this.previousVelocity = previousVelocity;
   }

   /**
    * Set velocity.
    *
    * @param velocity the velocity to set
    */
   public void setVelocity(double velocity) {
      this.velocity = velocity;
   }

   /**
    * Set position.
    *
    * @param position the position to set
    */
   public void setPosition(double position) {
      this.position = position;
   }

   /**
    * Set isNitroUsed.
    *
    * @param isNitroUsed the isNitroUsed to set
    */
   public void setNitroUsed(boolean isNitroUsed) {
      this.isNitroUsed = isNitroUsed;
   }

   @Override
   public String toString() {
      return "FormulaOneCar [team=" + team + ", topSpeed=" + topSpeed + ", acceleration=" + acceleration + ", previousVelocity="
               + previousVelocity + ", velocity=" + velocity + ", position=" + position + ", isNitroUsed=" + isNitroUsed + "]";
   }

}
