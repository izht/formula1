package izht.agoda.formula1;

import izht.agoda.formula1.exception.RaceExcepion;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Application entry point.
 * 
 * @author Boonyachart Kanchanabul
 */
public class App {

   public static void main(String[] args) {
      int numberOfTeam;
      double trackLength;
      System.out.println("Welcome to Formula-1 racing !");
      Scanner scanner = null;
      try {
         if (args.length == 0) {
            System.out.println("How many team in this race?");
            scanner = new Scanner(System.in);
            numberOfTeam = scanner.nextInt();
            System.out.println("How long is this track in meters?");
            trackLength = scanner.nextInt();
            scanner.close();
            scanner = null;
         }
         else {
            numberOfTeam = Integer.parseInt(args[0]);
            trackLength = Double.parseDouble(args[1]);
            System.out.println(String.format("There are %d participated in the track of %.2f meters", numberOfTeam, trackLength));
         }
         Race race = new Race(numberOfTeam, trackLength);
         System.out.println("Let's the game begin !!");
         race.start();
         while (race.numberOfRacingCar() > 0) {
            race.reAssessment();
            race.endReAssessment();
         }
      }
      catch (RaceExcepion e) {
         System.err.println("Error : " + e.getMessage());
      }
      catch (InputMismatchException | NumberFormatException | NullPointerException e) {
         System.out.println("Error : Invalid input");
      }
      finally {
         if (scanner != null)
            scanner.close();
      }
   }
}
