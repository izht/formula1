# Formula 1 Challenge
### Rule Summary
- There are n teams numbered 1 to n, i is the team number
- Top speed : (150 + 10 * i)
- Acceleration : (2 * i)
- Handling factor when car get near each other in 10 meters : 0.8*current speed
- Last car will use nitro, each car can only use once : 2*current speed
- Start Position : 200 * i behind the i-1 position.
- A re-assessment of the positions is done every 2 seconds
### Assumption
- A track length must be long enough to place all cars
- The last car is at 0 meters from start line, while others are in front of it.
- The first car might start at the finish line, and still in the race until the first re-assessment
- The first re-assessment is done at 2 second from start
- Handling factor and Nitro will change speed instantly during re-assessment
- The car keep accelerate at all time
- The race end when all car pass the finish line

### Flow Chart
![alt text](https://bitbucket.org/izht/formula1/downloads/Flowchart.png "Flowchart")

### How to run
Download the JAR from https://bitbucket.org/izht/formula1/downloads/formula1-1.0.jar then run

```sh
java -jar formula1-1.0.jar [team number] [track length]
```

`[team number]` and `[track length]` are optional, if not provide the program will ask for user input during running the program

You also can build new JAR from source code using `mvn package`