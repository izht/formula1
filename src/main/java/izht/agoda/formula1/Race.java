package izht.agoda.formula1;

import izht.agoda.formula1.exception.RaceExcepion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * A race.
 * 
 * @author Boonyachart Kanchanabul
 */
public class Race {

   private static final int ASSESSMENT_TIME = 2;
   private static final int RANGE_TO_APPLY_HANDLING_FACTOR = 10;

   private List<FormulaOneCar> cars = new ArrayList<>();
   private int totalTime;
   private double trackLength;

   public Race(int numberOfTeam, double trackLength) throws RaceExcepion {
      if (numberOfTeam < 1)
         throw new RaceExcepion("Participated team must be more than zero");

      // create car
      FormulaOneCar previousCar = null;
      for (int i = 1; i <= numberOfTeam; i++) {
         FormulaOneCar car = new FormulaOneCar(i, previousCar);
         cars.add(car);
         previousCar = car;
      }

      // move all car into the track
      double lastCarPosition = cars.get(numberOfTeam - 1).getPosition();
      if (lastCarPosition * -1 > trackLength)
         throw new RaceExcepion(String.format("Track too short we need at least %.0f meters", (lastCarPosition * -1)));
      for (FormulaOneCar car : cars) {
         car.setPosition(car.getPosition() - lastCarPosition);
      }
      this.trackLength = trackLength;
   }

   /**
    * All of them start at the same time and try to attain their top speed.
    */
   public void start() {
      totalTime += ASSESSMENT_TIME;
      for (FormulaOneCar car : cars) {
         car.accelerate(ASSESSMENT_TIME);
         car.drive(ASSESSMENT_TIME);
      }
   }

   /**
    * Remove finished cars from the race.
    * 
    * @return remaining cars in race
    */
   public int numberOfRacingCar() {
      for (Iterator<FormulaOneCar> iterator = cars.iterator(); iterator.hasNext();) {
         FormulaOneCar car = iterator.next();
         if (car.getPosition() > trackLength) {
            // Print output result
            System.out.println(String.format("Team number %d finished with final speed of %.2f in %d seconds", car.getTeam(),
                     car.getVelocity(), totalTime));
            iterator.remove();
         }
      }
      return cars.size();
   }

   /**
    * A re-assessment of the positions is done every 2 seconds.
    */
   public void reAssessment() {
      totalTime += ASSESSMENT_TIME;
      Collections.sort(cars);

      // calculate car velocity
      int i = 0;
      FormulaOneCar car = cars.get(i);
      FormulaOneCar previous = null;
      FormulaOneCar next = null;
      do {
         // find next car
         next = ++i < cars.size() ? cars.get(i) : null;

         if (previous != null && previous.getPosition() - car.getPosition() <= RANGE_TO_APPLY_HANDLING_FACTOR)
            car.handling();
         else if (next != null && car.getPosition() - next.getPosition() <= RANGE_TO_APPLY_HANDLING_FACTOR)
            car.handling();

         if (next == null)
            car.nitro();

         car.accelerate(ASSESSMENT_TIME);

         // reAssessment next car
         previous = car;
         car = next;
      }
      while (car != null);
   }

   /**
    * Move all car after apply new velocity.
    */
   public void endReAssessment() {
      for (FormulaOneCar car : cars) {
         car.drive(ASSESSMENT_TIME);
      }
   }

   /**
    * Gets cars.
    *
    * @return the cars
    */
   public List<FormulaOneCar> getCars() {
      return cars;
   }

   /**
    * Set cars.
    *
    * @param cars the cars to set
    */
   public void setCars(List<FormulaOneCar> cars) {
      this.cars = cars;
   }

   /**
    * Gets totalTime.
    *
    * @return the totalTime
    */
   public int getTotalTime() {
      return totalTime;
   }

   /**
    * Set totalTime.
    *
    * @param totalTime the totalTime to set
    */
   public void setTotalTime(int totalTime) {
      this.totalTime = totalTime;
   }

   /**
    * Gets trackLength.
    *
    * @return the trackLength
    */
   public double getTrackLength() {
      return trackLength;
   }

   /**
    * Set trackLength.
    *
    * @param trackLength the trackLength to set
    */
   public void setTrackLength(int trackLength) {
      this.trackLength = trackLength;
   }

}
