package izht.agoda.formula1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Test FormulaOneCar.
 * 
 * @author Boonyachart Kanchanabul
 */
public class FormulaOneCarTest {

   @Test
   public void testCreate() {
      FormulaOneCar dummy = new FormulaOneCar(10, null);
      dummy.setPosition(100000);
      FormulaOneCar car = new FormulaOneCar(11, dummy);
      assertEquals("Top speed = 150 + 10 * i", 150 + 10 * 11, car.getTopSpeed());
      assertEquals("Acceleration = 2 * i", 2 * 11, car.getAcceleration(), 0);
      assertEquals("Position = (position of i-1) - (200 * i)", 100000 - 200 * 11, car.getPosition(), 0);
   }

   @Test
   public void testHandling() {
      FormulaOneCar dummy = new FormulaOneCar(10, null);
      dummy.setVelocity(100);
      dummy.handling();
      assertEquals("Handling Factor = 0.8", 0.8 * 100, dummy.getVelocity(), 0);
   }

   @Test
   public void testNitro() {
      FormulaOneCar dummy = new FormulaOneCar(10, null);
      dummy.setVelocity(100);
      dummy.setTopSpeed(300);
      dummy.nitro();
      assertEquals("Nitro must double the speed", 200, dummy.getVelocity(), 0);
      dummy.nitro();
      assertEquals("Nitro can only use once", 200, dummy.getVelocity(), 0);
   }

   @Test
   public void testNitroLimit() {
      FormulaOneCar dummy = new FormulaOneCar(10, null);
      dummy.setVelocity(100);
      dummy.setTopSpeed(150);
      dummy.nitro();
      assertEquals("Nitro must not go beyond top speed", 150, dummy.getVelocity(), 0);
   }

   @Test
   public void testTopSpeedLimit() {
      FormulaOneCar dummy = new FormulaOneCar(1, null);
      while (dummy.getVelocity() < dummy.getTopSpeed()) {
         dummy.accelerate(2);
         dummy.drive(2);
      }
      assertTrue("Car can't go faster than top speed", dummy.getVelocity() == dummy.getTopSpeed());
      dummy.accelerate(2);
      dummy.drive(2);
      assertTrue("Car at top speed can't go faster", dummy.getVelocity() == dummy.getTopSpeed());
   }
}
