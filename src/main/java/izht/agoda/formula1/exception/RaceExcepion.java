package izht.agoda.formula1.exception;

/**
 * Race Exception.
 * 
 * @author Boonyachart Kanchanabul
 */
public class RaceExcepion extends Exception {

   private static final long serialVersionUID = -4702631015707532210L;

   public RaceExcepion(String message) {
      super(message);
   }
}
