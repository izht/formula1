package izht.agoda.formula1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import izht.agoda.formula1.exception.RaceExcepion;

import java.util.List;

import org.junit.Test;

/**
 * Race test.
 * 
 * @author Boonyachart Kanchanabul
 */
public class RaceTest {

   @Test(expected = RaceExcepion.class)
   public void testInvalidTeamNumber() throws RaceExcepion {
      int numberOfTeam = 0;
      int trackLength = 10000;
      new Race(numberOfTeam, trackLength);
   }

   @Test(expected = RaceExcepion.class)
   public void testTooShortTrack() throws RaceExcepion {
      int numberOfTeam = 2;
      int trackLength = 10;
      new Race(numberOfTeam, trackLength);
   }

   @Test
   public void testCreateRace() throws RaceExcepion {
      int numberOfTeam = 3;
      int trackLength = 1000;
      Race race = new Race(numberOfTeam, trackLength);
      assertEquals("Number of cars must equal to number of team", numberOfTeam, race.getCars().size());
      assertEquals("Tracklength must match with input", trackLength, race.getTrackLength(), 0);

      // The start line for (i + 1)th car is 200 * i meters behind the ith car.
      List<FormulaOneCar> cars = race.getCars();
      assertEquals("Car#1 must start at 1000 meters", 1000, cars.get(0).getPosition(), 0);
      assertEquals("Car#2 must start at 600 meters", 600, cars.get(1).getPosition(), 0); // 200*2 meters behind
                                                                                         // first car
      assertEquals("Last car(Car#3) must start at 0 meters", 0, cars.get(2).getPosition(), 0); // 200*3 meters
                                                                                               // behind second
                                                                                               // car
   }

   @Test
   public void testStart() throws RaceExcepion {
      int numberOfTeam = 3;
      int trackLength = 2000;
      Race race = new Race(numberOfTeam, trackLength);
      race.start();
      List<FormulaOneCar> cars = race.getCars();
      assertFalse("No one use nitro before the first Assessment", cars.get(0).isNitroUsed());
      assertFalse("No one use nitro before the first Assessment", cars.get(1).isNitroUsed());
      assertFalse("No one use nitro before the first Assessment", cars.get(2).isNitroUsed());

      // Acceleration: (2 * i) meter per second square.
      // So, velocity = acceleration * second
      assertEquals("Car#1 must has velocity at 4", (2 * 1) * 2, cars.get(0).getVelocity(), 0);
      assertEquals("Car#1 must has velocity at 8", (2 * 2) * 2, cars.get(1).getVelocity(), 0);
      assertEquals("Car#1 must has velocity at 12", (2 * 3) * 2, cars.get(2).getVelocity(), 0);
   }

   @Test
   public void testNitroInReAssessment() throws RaceExcepion {
      int numberOfTeam = 3;
      int trackLength = 2000;
      Race race = new Race(numberOfTeam, trackLength);
      List<FormulaOneCar> cars = race.getCars();
      race.start();
      race.reAssessment();
      assertFalse("Only last car use nitro", cars.get(0).isNitroUsed());
      assertFalse("Only last car use nitro", cars.get(1).isNitroUsed());
      assertTrue("Only last car use nitro", cars.get(2).isNitroUsed());
   }

   @Test
   public void testHandlingInReAssessment() throws RaceExcepion {
      int numberOfTeam = 3;
      int trackLength = 20000;
      Race race = new Race(numberOfTeam, trackLength);
      race.start();

      // inject position to test handling
      List<FormulaOneCar> cars = race.getCars();
      cars.get(0).setPosition(121);
      cars.get(1).setPosition(110);
      cars.get(2).setPosition(100);
      // disable nitro for easier test
      cars.get(0).setNitroUsed(true);
      cars.get(1).setNitroUsed(true);
      cars.get(2).setNitroUsed(true);
      race.reAssessment();

      // decrease by handling velocity = velocity * FormulaOneCar.HANDLING_FACTOR
      // normal accelrate velocity = velocity + acceleration * second;
      System.out.println(cars.get(0).getPreviousVelocity() + cars.get(0).getAcceleration() * 2);
      System.out.println(cars.get(0).getVelocity());
      assertTrue("Speed of Car#1 must NOT decrease by handling factor", //
               cars.get(0).getPreviousVelocity() + cars.get(0).getAcceleration() * 2 == cars.get(0).getVelocity());
      assertTrue("Speed of Car#2 must be decrease by handling factor", //
               cars.get(1).getPreviousVelocity() + cars.get(1).getAcceleration() * 2 > cars.get(1).getVelocity());
      assertTrue("Speed of Car#3 must be decrease by handling factor", //
               cars.get(2).getPreviousVelocity() + cars.get(2).getAcceleration() * 2 > cars.get(2).getVelocity());
   }

   @Test
   public void testFinish() throws RaceExcepion {
      int numberOfTeam = 3;
      int trackLength = 1000;
      Race race = new Race(numberOfTeam, trackLength);
      race.start();
      race.reAssessment();
      race.endReAssessment();
      race.numberOfRacingCar();
      List<FormulaOneCar> cars = race.getCars();
      assertEquals("Only 2 car left in the race", 2, cars.size());
      assertTrue("Racing car position must less than trackLength", cars.get(0).getPosition() < trackLength);
      assertTrue("Racing car position must less than trackLength", cars.get(1).getPosition() < trackLength);
   }

}
